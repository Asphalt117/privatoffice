﻿using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.ModelView;
using System.Web.Mvc;
using System;
using System.Data.Entity;
using Domain.Engine;
using System.Threading.Tasks;
using System.Data;

namespace Domain.Repository
{
    public class OrderRepository
    {
        private AbzContext db = new AbzContext();
        private OrderView sv = new OrderView();
        private PersonRepository personRepo = new PersonRepository();

        public async Task<List<OrderV>> GetOrder(int id)
        {
            List<OrderV> orders = await db.OrderVs.Where(d => d.CustId == id && d.StatusId>0).OrderByDescending(x => x.Dat).ToListAsync();
            return orders;
        }
        public async Task<int> Delete(int id)
        {
            Order ord = await db.Orders.FindAsync(id);
            db.Orders.Remove(ord);
            await db.SaveChangesAsync();
            return id;
        }
        public async Task<OrderView> GetChange(int id)
        {
            OrderV vsh = await db.OrderVs.FindAsync(id);
                
            sv.OrderId = vsh.OrderId;
            sv.CustId = vsh.CustId;
            sv.Good = vsh.Good;
            sv.Unit = vsh.Unit;
            sv.AdresId = vsh.AdresId;
            sv.Centr = vsh.Centr;
            sv.Dat = vsh.Dat;
            sv.CDat = DateToString.CDat(vsh.Dat);
            sv.Note = vsh.Note;
            sv.Status = vsh.Status;
            sv.PersonId = vsh.PersonId;
            sv = await GetSelectList(sv);
            sv.isOnlinePay = vsh.isOnlinePay;
            sv.Detail = db.OrderDetailViews.Where(o => o.OrderId == sv.OrderId).ToList();
            return sv;
        }
        public async Task<OrderView> GetCopy(int id)
        {
            sv = await GetChange(id);
            sv.OrderId = 0;
            sv.Dat = DateTime.Now;
            sv.CDat = DateToString.CDat(sv.Dat);
            int Orderid=await Save(sv);
            sv.OrderId = Orderid;
            foreach (var item in sv.Detail)
            {
                OrderDetail detail = new OrderDetail();
                detail.OrderDetailId = 0;
                detail.GoodId = item.GoodId;
                detail.OrderId = sv.OrderId;
                detail.Quant = item.Quant;
                db.OrderDetails.Add(detail);
            }            
            await db.SaveChangesAsync();
            
            return await GetChange(Orderid);
        }        
        public async Task<OrderView> GetCreate(int custid)
        {
            sv.CustId = custid;
            sv.AdresId = 1;
            sv.PersonId = 0;
            sv = await GetSelectList(sv);
            sv.Dat = DateTime.Now;
            sv.CDat = DateToString.CDat(sv.Dat);
            sv.Detail = new List<OrderDetailView>();
            sv.OrderId = await Save(sv,sv.Detail);

            return sv;
        }
        private async Task<OrderView> GetSelectList(OrderView order)
        {
            int CustId = order.CustId;
            Adres adres = await db.Adreses.FindAsync(order.AdresId);
            sv.Adres = adres.txt;
            Contract contract = await db.Contracts.FindAsync(order.ContractId);
            sv.Contract = contract.Num;            
            Person person = await db.Persons.FindAsync(order.PersonId);
            sv.Person = person.Name;
            sv.SelectAdres = new SelectList(await db.Adreses.Where(a => a.CustId == CustId | a.CustId == null).ToListAsync(), "AdresId", "Txt", sv.AdresId);
            sv.SelectContract = new SelectList(await db.Contracts.Where(a => a.CustID == CustId | a.CustID == 0).ToListAsync(), "ContractId", "Num", sv.ContractId);
            sv.SelectPerson = new SelectList( await personRepo.GetPerson(order.CustId), "PersonId", "Name", sv.PersonId);
            
            return sv;
        }

        public async Task<int> Save(OrderView sv)
        {
            Order order = new Order();
            order.OrderId = sv.OrderId;
            order.CustId = sv.CustId;
            order.AdresId = sv.AdresId;
            order.Centr = sv.Centr;
            order.ContractId = sv.ContractId;
            order.Dat = StringToDate.Date(sv.CDat);
            order.PersonId = sv.PersonId;
            order.note = sv.Note;
            order.insDate = DateTime.Now;
            order.isOnlinePay = sv.isOnlinePay;
            order.StatusId = sv.StatusId;
            if (order.OrderId == 0)
                db.Orders.Add(order);
            else
                db.Entry(order).State = EntityState.Modified;

            await db.SaveChangesAsync();
            return order.OrderId;
        }

        public async Task<int> Save(OrderView sv, List<OrderDetailView> det)
        {
            int id= await Save(sv);
            await SaveDetails(id, det);
            return id;
        }
        public async Task<int> SaveDetails(int id, List<OrderDetailView> det)
        {
            foreach (var item in det)
            {
                OrderDetail detal = new OrderDetail();
                detal.OrderDetailId = item.OrderDetailId;
                detal.GoodId = item.GoodId;
                detal.OrderId = id;
                detal.Quant = item.Quant;
                if (detal.OrderDetailId == 0)
                    db.OrderDetails.Add(detal);
                else
                    db.Entry(detal).State = EntityState.Modified;
            }
            int iddet = await db.SaveChangesAsync();
            return iddet;
        }
        public async Task<int> SaveDetail(int id, OrderDetailView det)
        {
            OrderDetail detal = new OrderDetail();
            detal.OrderDetailId = det.OrderDetailId;
            detal.GoodId = det.GoodId;
            detal.OrderId = id;
            detal.Quant = det.Quant;
            if (detal.OrderDetailId == 0)
                db.OrderDetails.Add(detal);
            else
                db.Entry(detal).State = EntityState.Modified;
            int iddet = await db.SaveChangesAsync();
            return iddet;
        }

    }
}
