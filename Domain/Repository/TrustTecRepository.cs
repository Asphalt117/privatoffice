﻿using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.ModelView;

namespace Domain.Repository
{
    public class TrustTecRepository 
    {
        private readonly AbzContext db=new AbzContext();

        //Получение действующих доверенностей по фирме
        public IEnumerable<OrgTrustTec> GetTrustActing(int custID)
        {
            return db.OrgTrustTecs.Where(t => t.CustId == custID);
        }
        //Получение ранее использовавшегося списка транспорта
        public IEnumerable<DrivTec> GetDrivTecs(int custID)
        {
            return db.DrivTecs.Where(d => d.CustId == custID);
        }
        //Получение ранее использовавшегося списка Гос №
        public IEnumerable<TecGn> GetGn(int custId)
        {
            IEnumerable<DrivTec> drivTecs = GetDrivTecs(custId);
            List<TecGn> gn = new List<TecGn>();
            var gosnum = from d in drivTecs
                         group d by d.Gn into g
                         select new
                         {
                             Gn = g.Key,
                             TecModel = g
                             //from m in g group m by 
                         };
            foreach (var gnum in gosnum)
            {
                TecGn tgn = new TecGn();
                tgn.Gn = gnum.Gn;
                tgn.TecModel = gnum.TecModel.First(c => c.Gn == gnum.Gn).TecModel;
                gn.Add(tgn);
            }
            return gn;
        }

        //Получение конкретного транспорта
        public DrivTec GetDrivTec(int id)
        {
            return db.DrivTecs.Find(id);
        }
        //Добавление транспорта в текущую доверенность
        public TrustTecDet DrivToTrustTecDets(int id)
        {
            //Перенос из DrivTec в TrustTecDet
            TrustTecDet ttd = new TrustTecDet();
            DrivTec drivTec = GetDrivTec(id);

            ttd.Driv = drivTec.Driv;
            ttd.DrivTecId = id;
            ttd.Gn = drivTec.Gn;
            ttd.TecModel = drivTec.TecModel;

            return ttd;
        }

        //public TrustTecView Add(TrustTecView tec, List<TrustTecDet> trustTecDets)
        //{
        //    TrustTec trustTec = new TrustTec();

        //    trustTec.BeginDat = tec.Bdat;
        //    trustTec.EndDat = tec.Edat;

        //    trustTec.CustId = custID;
        //    trustTec.Note = tec.Note;
        //    db.TrustTecs.Add(trustTec);
        //    db.SaveChanges();

        //    foreach (TrustTecDet trustTecDet in trustTecDets)
        //    {
        //        trustTecDet.TrustTecId = trustTec.TrustTecId;
        //        db.TrustTecDets.Add(trustTecDet);
        //    }
        //    db.SaveChanges();

        //    TrustTecView ttView = tec;
        //    ttView.TrustTecDets = trustTecDets;
        //    return ttView;
        //}

        //public IEnumerable<TecModel> GetTecModel()
        //{
        //    return db.TecModels;
        //}

        //public TecModel GetTecModel(int id)
        //{
        //    return db.TecModels.Find(id);
        //}
    }
}