﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Domain.Entities;

namespace Domain.ModelView
{
    public class TrustTecView
    {
        [Key]
        public int TrastTecId { get; set; }
        [DisplayName("Начало")]
        [Required]
        public string Bdat { get; set; }
        [DisplayName("Окончание")]
        [Required]
        public string Edat { get; set; }
        [DisplayName("Примечание")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        public List<TrustTecDet> TrustTecDets { get; set; }
    }
}