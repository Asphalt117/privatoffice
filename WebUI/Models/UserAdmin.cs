﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebUI.Models
{
    public class UserAdmin
    {
        public string id { get; set; }
        public string UserName { get; set; }
        public int CustID { get; set; }
    }
    public class RegisterAdmin
    {
        [Required]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Cod1s")]
        public string Cod1s { get; set; }
    }

}