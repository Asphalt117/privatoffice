﻿using System;
using Domain.Entities;
using System.Collections.Generic;

namespace WebUI.Models
{
    public class AssortmentMenu
    {
        public string MenuName { get; set; }
        public int? MenuId { get; set; }
        public IEnumerable<Categ> Categs { get; set; }
    }
}