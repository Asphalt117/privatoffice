﻿using Domain.Entities;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace WebUI.Controllers
{
    public class DocsController : BaseController
    {
        public ActionResult Index()
        {
            Menu.ChangeSelected(2, 1);
            Session["Menu"] = Menu;
            List<Trust> trusts = db.Trusts.Where(t => t.CustId == Menu.CustId).ToList();
            return View(trusts);
        }
        public ActionResult Contract()
        {
            Menu.ChangeSelected(2, 2);
            Session["Menu"] = Menu;
            List<Contract> contracts = db.Contracts.Where(t => t.CustID == Menu.CustId).ToList();                
            return View(contracts);
        }
        public ActionResult SchFact()
        {
            Menu.ChangeSelected(2, 3);
            Session["Menu"] = Menu;
            ViewBag.mail = User.Identity.Name;                
            return View();
        }
        [HttpPost]
        public ActionResult SchFact(string note)
        {
            OrdInvoice ord = new OrdInvoice();
            ord.CustId = Menu.CustId;
            ord.Note = note;
            db.OrdInvoices.Add(ord);
            db.SaveChanges();

            ViewBag.mail = User.Identity.Name;
            return View("SchFactZak");
        }
    }
}
