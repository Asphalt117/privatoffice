﻿using System.Web.Mvc;
using Domain.Entities;
using Domain.Repository;
using System.Collections.Generic;
using Domain.ModelView;
using System.Linq;

namespace WebUI.Controllers
{
    public class TrustTecController : BaseController
    {
        //Сразу показывать список выбранной техники.
        //При создании переходим на спосок использованных машин.
        //Показываем даты и список выбюранных, с возможностью добавить

        private TrustTecRepository repo=new TrustTecRepository();

        public ActionResult Index()
        {
            //Показываю действующие доверенности
            return View(repo.GetTrustActing(Menu.CustId));
        }
        public ActionResult CreateTec(TrustTecDet trustTecDet)
        {
            TrustTecDet ttDet;
            if (trustTecDet == null)
                ttDet = new TrustTecDet();
            else
                ttDet = trustTecDet;
            return View(ttDet);
        }
        public ActionResult GNSelect()
        {
            repo = new TrustTecRepository();
            List<TecGn> gn = repo.GetGn(Menu.CustId).ToList();
            return View(gn);
        }
        public ActionResult AddGN(string id)
        {
            repo = new TrustTecRepository();
            TrustTecDet ttDet = new TrustTecDet();
            ttDet.Gn = id;
            ttDet.TecModel = repo.GetDrivTecs(Menu.CustId).First(c => c.Gn == id).TecModel;
            return RedirectToAction("CreateTec", "TrustTec", ttDet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTec(TrustTecDet trustTecDet,string qq)
        {
            if (ModelState.IsValid)
            {
                TrustTecDet ttDet = trustTecDet;
                db.TrustTecDetss.Add(trustTecDet);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View();
        }

        //public ActionResult TecModelSelect()
        //{
        //    return View(repo.GetTecModel(Menu.CustId));
        //}

        //public ActionResult AddTecModel(int id)
        //{
        //    repo = new TrustTecRepository(Cust.OrgCustID);
        //    TrustTecDet ttDet;
        //    TecModel tm = repo.GetTecModel(id);
        //    if (Session["CurTrustTecDet"] != null)
        //    {
        //        ttDet = (TrustTecDet) Session["CurTrustTecDet"];
        //    }
        //    else
        //    {
        //        ttDet=new TrustTecDet();
        //    }
        //    ttDet.TecModel = tm.Name;

        //    Session["CurTrustTecDet"] = ttDet;
        //    return RedirectToAction("CreateTec");
        //}





        //private TrustTecView fillTecView(TrustTecDet ttd)
        //{

        //    TrustTecDet query;    
        //    List<TrustTecDet> ttDets;
        //    if (Session["TrustTecDet"] != null)
        //    {
        //        ttDets = (List<TrustTecDet>)Session["TrustTecDet"];

        //        //Найти наименьший id
        //        query = ttDets.Last();
        //        ttd.TrustTecDetId = query.TrustTecDetId - 1;
        //    }
        //    else
        //    {
        //        ttDets = new List<TrustTecDet>();
        //        ttd.TrustTecDetId = -1;
        //    }

        //    ttDets.Add(ttd);
        //    Session["TrustTecDet"] = ttDets;

        //    TrustTecView ttView = new TrustTecView();
        //    ttView.TrustTecDets = ttDets;
        //    return ttView;
        //}

        //public ActionResult Delete(int id)
        //{
        //    TrustTecView ttView = new TrustTecView();
        //    if (Session["TrustTecDet"] != null)
        //    {
        //        List<TrustTecDet> ttDets = (List<TrustTecDet>)Session["TrustTecDet"];

        //        TrustTecDet ttd = ttDets.Find(n=>n.TrustTecDetId==id);

        //        if (ttd!=null)
        //            ttDets.Remove(ttd);
        //        Session["TrustTecDet"] = ttDets;
        //        ttView.TrustTecDets = ttDets;
        //    }
        //    return View("Addtec", ttView);
        //}

        //public ActionResult AddTec(int id)
        //{
        //    //Добавление транспорта в текущую доверенность
        //    //Перенос из DrivTec в TrustTecDet
        //    repo = new TrustTecRepository(Cust.OrgCustID);
        //    TrustTecDet ttd = repo.DrivToTrustTecDets(id);

        //    return View(fillTecView(ttd));
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult AddTec(TrustTecView tec)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        repo = new TrustTecRepository(Cust.OrgCustID);
        //        TrustTecView ttView = repo.Add(tec, (List<TrustTecDet>) Session["TrustTecDet"]);
        //        return View("AddTecSucces",ttView);
        //    }
        //    return View();
        //}
    }
}