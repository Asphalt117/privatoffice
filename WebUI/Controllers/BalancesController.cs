﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Domain.ModelView;
using WebUI.Models;
using Domain.Repository;

namespace WebUI.Controllers
{
    public class BalancesController : BaseController
    {
        public int year = DateTime.Today.Year;
        public int dm = DateTime.Today.Month;
        private static int pagesize = 20;          // Items count on one page
        private static int vsblpagescount = 10;    // Visible page numbers in page navigator central list 


        public void SetDat(int? selectedId, int? Id)
        {
            if (Id != null)
                year = (int)Id;
            else
            {
                if (Session["Year"] != null)
                    year = (int)Session["Year"];
                else
                    year = DateTime.Today.Year;
            }
            if (selectedId != null)
                dm = (int)selectedId;
            else
            {
                if (Session["Month"] != null)
                    dm = (int)Session["Month"];
                else
                    dm = DateTime.Today.Month;
            }
            Session["Month"] = dm;
            Session["Year"] = year;
        }
        [Authorize]
        public ActionResult Index(int? selectedId, int? Id, int pageNum = 1)
        {
            Menu.ChangeSelected(4, 4);
            Session["Menu"] = Menu;
            SetDat(selectedId, Id);
            DateOf datof = new DateOf(dm, year);
            ViewData["Month"] = new SelectList(datof.getMonth, "ID", "Mnth", dm);
            ViewData["Year"] = new SelectList(datof.getYear, "ID", "Mnth", year);
            return View();
        }
        public ActionResult SelectTtn(int? selectedId, int? Id, int pageNum = 0)
        {
            TtnRepository repo = new TtnRepository();
            SetDat(selectedId, Id);
            //Установили 1е число
            DateTime begDt = new DateTime(year, dm, 01, 0, 0, 0);
            //Установили 1е число следующего месяца
            DateTime endDt = begDt.AddMonths(1);
            TtnList ttn = new TtnList();
            int count = repo.GetCount(Menu.CustId, begDt, endDt);
            ttn.PageInfo = new PageInfo { pageNum = pageNum, itemsCount = count, pageSize = pagesize, vsblPagesCount = vsblpagescount };
            ttn.Ttns = repo.GetSkipTake((pageNum)*pagesize, pagesize, Menu.CustId, begDt, endDt).ToList();
            return PartialView(ttn);
        }
        [Authorize]
        public ActionResult Balance()
        {
            BalanceRepository repo = new BalanceRepository();
            //MyMenu menu = (MyMenu)Session["Menu"];
            Menu.ChangeSelected(3, 0);
            Session["Menu"] = Menu;
            return View(repo.GetListBalance(Menu.CustId));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
